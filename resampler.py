# Copyright (c) 2014 Alexander E. Patrakov. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import numpy as np

from utils import PFFT

import logging


class FFTBinTracker(object):
    """
    Auxiliary class that encapsulates the task of tracking the main
    spectral component of a resampled linear sine sweep.
    """
    def __init__(self, fftbin_lo, fftbin_hi):
        """
        Initialize the bin tracker, using fftbin_lo as the bin index
        where the target signal power can be estimated, and using both
        bins to estimate the frequency sweep slope. FFT bins above
        fftbin_hi are tracked using the position of the signal piece in
        time rather than its spectrum, because they are expected to
        correspond to frequencies rejected by the resampler.
        Window is used to correct the signal power estimation.
        """
        self.fftbin_lo = fftbin_lo
        self.fftbin_hi = fftbin_hi
        self.start_lo = None
        self.start_hi = None
        self.target_power = None

    def get_fftbin(self, start, pfft):
        """
        Find the FFT bin that corresponds to this piece, using its
        power spectrum. Also update the estimation of the power that
        should be there in the signal.
        """
        if self.start_hi is not None:
            slope = (1.0 * (self.fftbin_hi - self.fftbin_lo)
                     / (self.start_hi - self.start_lo))
            return int(self.fftbin_lo + slope * (start - self.start_lo))
        else:
            fftbin = np.argmax(pfft)
            if self.start_lo is None:
                if fftbin >= self.fftbin_lo:
                    self.start_lo = start
                    self.target_power = np.sum(pfft)

            if self.start_hi is None:
                if fftbin >= self.fftbin_hi:
                    self.start_hi = start

            return fftbin


class DropoutDetector(object):
    """
    Encapsulates detection of signal presence and dropouts
    """
    def __init__(self, min_level, min_dropout_length):
        """
        Initializes the detector using the provided parameters:
        minimum PCM level cosidered as non-silence and the minimum
        length of run of near-zeros that should be considered to be
        a dropout.
        """
        self.min_level = min_level
        self.min_dropout_length = min_dropout_length

    def has_signal(self, pcm_piece):
        """
        Returns True if the pcm_piece is different enough from silence.
        """
        return np.max(np.abs(pcm_piece[:len(pcm_piece)/2])) >= self.min_level

    def has_dropout(self, pcm_piece):
        """
        Returns True if pcm_piece has enough consecutive near-zeros.
        """
        # Copied from https://stackoverflow.com/questions/1066758
        l = len(pcm_piece)
        piece = np.hstack(([1.0], pcm_piece[l/4:3*l/4], [1.0]))
        zero_places = (np.abs(piece) < self.min_level).astype(np.int)
        diffs = np.diff(zero_places)
        ups, = np.where(diffs == 1)
        downs, = np.where(diffs == -1)

        runs = np.hstack(([0], downs - ups))
        longest_run = np.max(runs)
        return longest_run >= self.min_dropout_length


class ResamplerResponse(object):
    """
    Collects a response matrix from an arbitrary wav file containing
    a resampled linear sweep.
    """
    def __init__(self, sample_rate, original_sample_rate, fft_size, wave):
        """
        Populates the following attributes:

        sample_rate: the sample rate that the sound was resampled to

        original_sample_rate: the sample rate that the sound was resampled from

        rate_ratio: ratio of the original and new sample rates

        response_matrix[i, j]: the power of the resample response in FFT bin j
        when given a full-amplitude sine wave with the frequency that would
        fall into the FFT bin i according to the new sample rate

        fftbin_max: the FFT bin corresponding to the Nyquist frequency at the
        original sample rate
        """
        self.sample_rate = sample_rate
        self.original_sample_rate = original_sample_rate
        self.fft_size = fft_size

        self.rate_ratio = 1.0 * original_sample_rate / sample_rate
        fftbin_lo = int(0.05 * self.rate_ratio * fft_size)
        fftbin_hi = int(0.35 * min(self.rate_ratio, 1) * fft_size)
        fftbin_max = int(fft_size * self.rate_ratio / 2)
        self.fftbin_max = fftbin_max

        if wave.dtype.kind == 'i':
            wave = wave.astype(np.float) / (np.iinfo(wave.dtype).max + 1)

        pfft = PFFT(fft_size / 2)

        piece_idx = 0

        dropout_detector = DropoutDetector(1 / 100.0, 30)
        tracker = FFTBinTracker(fftbin_lo, fftbin_hi)

        found_signal = False

        response_matrix = np.zeros(
            (fftbin_max + 1, fft_size / 2 + 1), dtype=np.float)
        representatives = np.zeros((fftbin_max + 1,), dtype=np.int)

        while True:
            start = piece_idx * fft_size / 2
            end = start + fft_size
            if end > len(wave):
                logging.warning("Out of data in piece starting at %d", start)
                break

            piece_idx += 1
            piece = wave[start:end]

            # First, find long runs of zeros, to catch any glitches introduced
            # by recording a wave from KVM
            if found_signal:
                if dropout_detector.has_dropout(piece):
                    logging.error("Found dropout in piece starting at %d",
                                  start)

            if not found_signal:
                if dropout_detector.has_signal(piece):
                    logging.info("Found signal at %d", start)
                    found_signal = True

            if not found_signal:
                continue

            spectrum = pfft.pfft(piece)
            fftbin = tracker.get_fftbin(start, spectrum)

            if fftbin > fftbin_max:
                logging.info("Out of useful data in piece starting at %d",
                             start)
                break

            representatives[fftbin] += 1
            response_matrix[fftbin, :] += spectrum

        # Avoid division by zero
        representatives = np.where(representatives == 0, 1, representatives)

        expected_power = representatives[:, np.newaxis] * tracker.target_power
        self.response_matrix = response_matrix / expected_power

    def to_model(self):
        return ResamplerModel(self)

    def to_audibility(self):
        return ResamplerAudibility(ResamplerModel(self))


class ResamplerModel(object):
    """
    Contains various quantities derived from the response matrix and only
    from it. I.e. anything that depends on the room noise or the signal
    amplitude or psychoacoustics does not belong here.
    """
    def __init__(self, resampler_response):
        self.sample_rate = resampler_response.sample_rate
        self.original_sample_rate = resampler_response.original_sample_rate
        self.fft_size = resampler_response.fft_size
        self.fftbin_max = resampler_response.fftbin_max

        self.rate_ratio = resampler_response.rate_ratio
        self.response_matrix = resampler_response.response_matrix
        self.distortion_matrix = None

        is_signal = np.fromfunction(
            lambda x, y: abs(x - y) <= 4,
            self.response_matrix.shape)
        self.response_envelope = np.sum(self.response_matrix * is_signal,
                                        axis=1)

        def is_fftbin_special(fftbin):
            if (fftbin <= 4):
                return True
            if abs(fftbin - self.fft_size / 2) <= 4:
                return True
            if fftbin >= self.fftbin_max - 4:
                return True
            return False

        signal_matrix = np.zeros_like(self.response_matrix)
        distortion_matrix = np.zeros_like(self.response_matrix)
        distortion_matrix_eq = np.zeros_like(self.response_matrix)

        # TODO: separate noise and distortion. Ideas how to do this:
        #
        # 1. With two wav files of different amplitude.
        #
        # 2. Using one wav file and hoping that the amount of noise
        #    at some frequency does not depend on the signal frequency.

        noise_median = np.median(self.response_matrix, axis=0)

        for fftbin in xrange(self.fftbin_max + 1):
            spectrum = self.response_matrix[fftbin, :]
            is_above_noise = spectrum > (noise_median * 3)
            spectrum *= is_above_noise
            wrong_amp = np.zeros_like(spectrum)

            if is_fftbin_special(fftbin):
                # Special bin with possibly strange amplitude.
                # Pretend that there is no distortion.
                signal_only = np.zeros_like(spectrum)
                distortion = np.zeros_like(spectrum)
            else:
                # Everything except the central peak is distortion.
                is_distortion = np.fromfunction(
                    lambda f: abs(f - fftbin) > 4,
                    (len(spectrum),))
                distortion = spectrum * is_distortion
                signal_only = spectrum * (1 - is_distortion)
                if fftbin < self.fft_size / 2:
                    # And also the wrong part of the central peak amplitude
                    # is a special form of distortion.
                    signal_power = np.sum(spectrum * (1 - is_distortion))
                    distortion_per_bin = np.abs((1.0 - signal_power) / 9)
                    wrong_amp = distortion_per_bin * (1 - is_distortion)

            signal_matrix[fftbin, :] = signal_only
            distortion_matrix[fftbin, :] = distortion + wrong_amp
            distortion_matrix_eq[fftbin, :] = distortion

        self.signal_matrix = signal_matrix
        self.distortion_matrix = distortion_matrix
        self.distortion_matrix_eq = distortion_matrix_eq


class ResamplerAudibility(object):
    def __init__(self, resampler_model, noise_model=None,
                 signal_db_fs_to_spl=None):
        from psymodel import PsyModel

        self.sample_rate = resampler_model.sample_rate
        self.original_sample_rate = resampler_model.original_sample_rate
        self.rate_ratio = 1.0 * self.original_sample_rate / self.sample_rate
        self.signal_matrix = resampler_model.signal_matrix
        self.distortion_matrix = resampler_model.distortion_matrix
        self.distortion_matrix_eq = resampler_model.distortion_matrix_eq

        self.response_plus_noise = np.zeros_like(resampler_model.response_matrix)

        self.fft_size = resampler_model.fft_size
        self.fftbin_max = resampler_model.fftbin_max

        if signal_db_fs_to_spl is not None:
            signal_scale = 10 ** ((signal_db_fs_to_spl - 92.0) / 10)
        else:
            signal_scale = 1.0

        model = PsyModel(self.sample_rate / 2, self.fft_size / 2)

        if noise_model is not None:
            model.set_noise_model(noise_model)

        audibility = np.zeros((self.fftbin_max + 1,), dtype=np.float)
        audibility_eq = np.zeros_like(audibility)

        for fftbin in xrange(self.fftbin_max + 1):
            signal_only = self.signal_matrix[fftbin, :] * signal_scale
            distortion = self.distortion_matrix[fftbin, :] * signal_scale
            distortion_eq = self.distortion_matrix_eq[fftbin, :] * signal_scale
            response = resampler_model.response_matrix[fftbin, :] * signal_scale

            if noise_model is not None:
                self.response_plus_noise[fftbin, :] = response + noise_model.pfft
            else:
                self.response_plus_noise[fftbin, :] = response

            audibility[fftbin] = model.distortion_audibility(
                signal_only, distortion)
            audibility_eq[fftbin] = model.distortion_audibility(
                signal_only, distortion_eq)

        self.audibility = audibility
        self.audibility_eq = audibility_eq
