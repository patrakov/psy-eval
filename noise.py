#!/usr/bin/env python2
# Copyright (c) 2014 Alexander E. Patrakov. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import numpy as np

from scipy.io import wavfile

from utils import PFFT

import argparse

def rebin(data, new_bins, range_stretch_factor=1.0):
    """
    The "data" NumPy array is assumed to describe a distribution of
    something in equal intervals ("bins")starting at 0. In each interval, the
    distribution is assumed to be uniform.

    This function calculates the distribution over bins of a different width.
    The "new_bins" parameter specified the total number of the new bins, and
    the "range_stretch_factor" parameter allows to change the endpoint of the
    rightmost interval.
    """
    old_bins = len(data)
    result = np.zeros(new_bins, dtype=data.dtype)
    x_per_new_bin = range_stretch_factor * old_bins / new_bins

    for bin in xrange(new_bins):
        x_low = x_per_new_bin * bin
        if x_low >= old_bins:
            break

        val = 0.0

        x_high = x_low + x_per_new_bin

        mid_xs = range(int(x_low + 1), int(x_high + 1))
        downs = [x_low] + mid_xs
        ups = mid_xs + [x_high]

        downups = zip(downs, ups)

        for down, up in downups:
            x = int(down)
            if x >= old_bins:
                break
            val += (up - down) * data[x]

        result[bin] = val
    return result


def dba_weigh(pspectrum, sample_rate):
    def dba_curve(f):
        result = (12200 * f * f) ** 2
        result /= f ** 2 + 20.6 ** 2
        result /= np.sqrt(f ** 2 + 107.7 ** 2)
        result /= np.sqrt(f ** 2 + 737.9 ** 2)
        result /= f ** 2 + 12200 ** 2
        return result + 1e-12

    fs = np.linspace(0, sample_rate / 2.0, len(pspectrum))
    dbas = dba_curve(fs) / dba_curve(1000.0)
    return pspectrum * (dbas ** 2)


class NoiseModel(object):
    """
    Keeps an average power of noise at each frequency, with the property
    that 1.0 means 92 dB SPL.
    """
    def __init__(self, sample_rate, fft_size, wave,
                 needed_rate=None, db_fs_to_spl=None, dba=None, sine=False):
        self.sample_rate = needed_rate
        self.fft_size = fft_size
        if fft_size % 2 == 1:
            raise ValueError("Odd FFT size is not supported")

        if needed_rate is None:
            needed_rate = sample_rate

        if db_fs_to_spl is not None and dba is not None:
            raise ValueError("It is an error to specify both the pressure "
                             "corresponding to the full scale and the noise "
                             "meter dBA reading.")

        if wave is None:
            # make an empty model
            self.pfft = np.zeros(fft_size / 2 + 1, dtype=np.float)
            return

        if wave.dtype.kind == 'i':
            wave = wave.astype(np.float) / (np.iinfo(wave.dtype).max + 1)

        # Force an even original FFT size to simplify the overlap code below
        orig_fft_size = int(0.5 * fft_size * sample_rate / needed_rate) * 2
        pfft = PFFT(orig_fft_size / 2)

        # Examine each sample twice to get more data. This is valid because
        # a window exists.
        npieces = 2 * len(wave) / orig_fft_size - 1
        if npieces < 20:
            raise ValueError("Not enough data for noise evaluation")

        pffts = np.zeros((npieces, orig_fft_size / 2 + 1), dtype=np.float)
        for piece_id in xrange(npieces):
            start = orig_fft_size / 2 * piece_id
            piece = wave[start:start + orig_fft_size]
            pffts[piece_id,:] = pfft.pfft(piece)

        # In an ideal situation we would calculate an average power in each
        # FFT bin. However, in reality, this is not robust to short periods
        # with some extra sound sources (e.g. ticking clock) that should
        # have no effect on masking resampler distortions. So use the median
        # and estimate the average from it.
        med_pfft = np.median(pffts, 0)

        # As the power in the FFT bin is distributed according to an
        # exponential distribution in the case of noise, we need to take the
        # known ratio between the median and the average into account.
        if sine:
            avg_pfft = med_pfft
        else:
            avg_pfft = med_pfft / np.log(2)

        # Scale the response in amplitude
        if db_fs_to_spl is not None:
            avg_pfft *= 10.0 ** ((db_fs_to_spl - 92.0) / 10)

        weighed_pfft = dba_weigh(avg_pfft, sample_rate)
        total_weighed_power = np.sum(weighed_pfft)
        self.total_weighed_power = total_weighed_power

        if dba is not None:
            wanted_weighed_power = 10.0 ** ((dba - 92.0) / 10)
            self.total_weighed_power = wanted_weighed_power
            avg_pfft *= wanted_weighed_power / total_weighed_power

        if needed_rate != sample_rate:
            avg_pfft = rebin(avg_pfft, fft_size / 2 + 1,
                             (fft_size / 2 + 1.0) / (orig_fft_size / 2 + 1.0))

        self.pfft = avg_pfft


def parse_args():
    parser = argparse.ArgumentParser(description='Noise meter')
    parser.add_argument('--fftsize', help='FFT size', type=int, default=1024)

    parser.add_argument('--noise-full-scale',
                        help='Sound pressure level, in dB, corresponding to the '
                        'full scale in the noise file (default: 92)',
                        type=float, default=92.0)
    parser.add_argument('--sine',
                        help='Assume that the input is a sine wave, not noise',
                        action='store_true', default=False)
    parser.add_argument('file', help='WAV file with noise')
    return parser.parse_args()

def main():
    args = parse_args()
    noise_rate, noise_wave = wavfile.read(args.file)
    if len(noise_wave.shape) == 2:
        noise_wave = noise_wave[:, 0]
    model = NoiseModel(noise_rate, args.fftsize, noise_wave,
                       db_fs_to_spl=args.noise_full_scale,
                       sine=args.sine)
    total_weighed_power = model.total_weighed_power
    print "The noise is %2.1f dB, A-weighed" % (
            92.0 + 10 * np.log10(total_weighed_power))


if __name__ == "__main__":
    main()
