# Copyright (c) 2014 Alexander E. Patrakov. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import numpy as np
from numpy.fft import rfft

class PFFT(object):
    def __init__(self, freq_bands):
        self.freq_bands = freq_bands
        self.window = self.proper_window(freq_bands)

    @staticmethod
    def proper_window(freq_bands):
        return np.kaiser(freq_bands * 2, 15.0)

    def pfft(self, pcm_wave):
        """
        Returns a version of the power spectrum of pcm_wave, suitably
        windowed and scaled.

        The scaling is done a such a way that, for a sine wave of amplitude 1,
        the sum of the coefficients in pfft output (i.e. the total power) is
        also normalized to 1.
        """
        assert len(pcm_wave) == self.freq_bands * 2
        spectrum = rfft(pcm_wave * self.window)
        factor = 2.0 * PFFT.reference_factor / (self.freq_bands ** 2)
        return (spectrum * spectrum.conjugate() * factor).real


example_bands = 4096
example_window = PFFT.proper_window(example_bands)
PFFT.reference_factor = example_bands / np.sum(example_window ** 2)

del example_bands
del example_window
