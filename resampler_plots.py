#!/usr/bin/env python2
# Copyright (c) 2014 Alexander E. Patrakov. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import numpy as np
import matplotlib.pyplot as p

from scipy.io import wavfile

from resampler import *
from noise import NoiseModel

import argparse


def parse_args():
    parser = argparse.ArgumentParser(description='Resampler tester')

    parser.add_argument('--rate-from',
                        help='Sample rate of the original signal',
                        type=int, required=True)
    parser.add_argument('--rate-to',
                        help='Sample rate of the resampled signal',
                        type=int)
    parser.add_argument('--skip',
                        help='Skip this number of input samples',
                        type=int, default=0)
    parser.add_argument('--fftsize', help='FFT size', type=int, default=1024)
    parser.add_argument('--noise-file', help='wav file with room noise')
    parser.add_argument('--noise-full-scale',
                        help='Sound pressure level, in dB, corresponding to the '
                        'full scale in the noise file (default: 92)',
                        type=float)
    parser.add_argument('--noise-dba',
                        help='A-weighed sound pressure level, in dB, corresponding '
                        'to the noise file (i.e. what the noise meter says)',
                        type=float)
    parser.add_argument('--signal-full-scale',
                        help='Sound pressure level, in dB, corresponding to the '
                        'full scale of the resampled signal (default: 92)',
                        type=float)
    parser.add_argument('--save', help='Prefix of the names of plot files')
    parser.add_argument('file', help='WAV file with resampler output')

    return parser.parse_args()


def save_plot(args, suffix):
    if not args.save:
        p.show()
    else:
        p.savefig(args.save + suffix, dpi=144)
        p.clf()


def create_noise_model(args, rate):
    if args.noise_file is None:
        noise_rate = rate
        noise_wave = None
    else:
        noise_rate, noise_wave = wavfile.read(args.noise_file)
        if len(noise_wave.shape) == 2:
            noise_wave = noise_wave[:, 0]

    return NoiseModel(noise_rate, args.fftsize, noise_wave, rate,
                      args.noise_full_scale, args.noise_dba)


def create_resampler_model(args, file):
    rate, sweep = wavfile.read(file)
    if len(sweep.shape) == 2:
        sweep = sweep[args.skip:, 0]

    if args.rate_to:
        rate = args.rate_to

    if not file:
        raise ValueError('WAV file with resampler response is required')

    rr = ResamplerResponse(rate, args.rate_from, args.fftsize, sweep)
    rm = rr.to_model()
    return rm


def main():
    args = parse_args()

    rm = create_resampler_model(args, args.file)
    rate = rm.sample_rate

    noise_model = create_noise_model(args, rate)

    ra = ResamplerAudibility(rm, noise_model, args.signal_full_scale)

    xs = np.linspace(0, args.rate_from / 2, rm.response_matrix.shape[0] + 1)
    ys = np.linspace(0, rate / 2, rm.response_matrix.shape[1] + 1)

    p.ylim(-30, 1)
    p.plot(xs[:-1], 10 * np.log10(rm.response_envelope))
    save_plot(args, '_envelope.png')

    p.pcolormesh(xs, ys, 10 * np.log10(rm.response_matrix.transpose()),
                 vmin=-120, vmax=0, cmap='hot')
    p.colorbar(format='%.0f dB')
    p.xlabel('Input frequency, Hz')
    p.ylabel('Output frequency, Hz')
    save_plot(args, '_response.png')

    p.pcolormesh(xs, ys, 10 * np.log10(ra.response_plus_noise.transpose()),
                 vmin=-120, vmax=0, cmap='hot')
    p.colorbar(format='%.0f dB')
    p.xlabel('Input frequency, Hz')
    p.ylabel('Output frequency, Hz')
    save_plot(args, '_response_plus_noise.png')

    p.pcolormesh(xs, ys, 10 * np.log10(rm.distortion_matrix.transpose()),
                 vmin=-120, vmax=0, cmap='hot')
    p.colorbar(format='%.0f dB')
    p.xlabel('Input frequency, Hz')
    p.ylabel('Output frequency, Hz')
    save_plot(args, '_distortion.png')

    p.pcolormesh(xs, ys, 10 * np.log10(rm.distortion_matrix_eq.transpose()),
                 vmin=-120, vmax=0, cmap='hot')
    p.colorbar(format='%.0f dB')
    p.xlabel('Input frequency, Hz')
    p.ylabel('Output frequency, Hz')
    save_plot(args, '_distortion_eq.png')

    p.ylim(-30, 20)
    p.plot(xs[:-1], 10 * np.log10(ra.audibility))
    p.axhline(0, color='k')
    p.xlabel('Input frequency, Hz')
    p.ylabel('Distortion audibility, dB')
    save_plot(args, '_audibility.png')

    p.ylim(-30, 20)
    p.plot(xs[:-1], 10 * np.log10(ra.audibility_eq))
    p.axhline(0, color='k')
    p.xlabel('Input frequency, Hz')
    p.ylabel('Distortion audibility, dB')
    save_plot(args, '_audibility_eq.png')


if __name__ == '__main__':
    main()
