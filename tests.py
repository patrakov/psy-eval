#!/usr/bin/env python2
# Copyright (c) 2014 Alexander E. Patrakov. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import unittest
import numpy as np

# some universal functions
from numpy import sin, pi
from numpy.fft import rfft

from utils import PFFT
from psymodel import PsyModel
from wavegen import generate_lin_sweep, to_s16
from resampler import *
from noise import rebin, NoiseModel

import logging


class TestPsyModel(unittest.TestCase):
    def test_ATH_dB(self):
        """
        Tests that ATH_dB() returns sensible values
        """
        self.assertAlmostEqual(PsyModel.ATH_dB(100), 23, delta=1)
        self.assertAlmostEqual(PsyModel.ATH_dB(1000), 3.5, delta=0.5)
        self.assertAlmostEqual(PsyModel.ATH_dB(3000), -4.0, delta=1)
        self.assertGreater(PsyModel.ATH_dB(20000), 90.0)

    def test_ATH(self):
        """
        Tests that ATH() returns sensible values:
        One-step wave at 16 bits is audible at 3000 Hz but not at 440 Hz
        """
        self.assertGreater(PsyModel.ATH(440), 1/32768.0)
        self.assertLess(PsyModel.ATH(3000), 1/32768.0)

    def test_gammatone_filter(self):
        """
        Tests that gamma-tone filters behave as they should
        """
        self.assertAlmostEqual(
            PsyModel.gammatone_filter(13, 1850), 1.0, delta=0.05)
        self.assertAlmostEqual(
            PsyModel.gammatone_filter(13, 1720), 0.6, delta=0.05)
        self.assertAlmostEqual(
            PsyModel.gammatone_filter(13, 2000), 0.6, delta=0.05)

    def test_filter_matrix(self):
        """
        Tests that the filter matrix really contains a properly sampled
        product of ATH and gamma-tone transfer function
        """
        filter_matrix = PsyModel.make_filter_matrix(24000, 128)
        self.assertEqual(filter_matrix.shape, (129, 26))

        # 10th frequency component (= 1875 Hz), 12th bark, starting from 0
        elem = filter_matrix[10, 12]
        should_be = (PsyModel.gammatone_filter(13, 1875)
                     / PsyModel.ATH(1875)) ** 2
        self.assertAlmostEqual(elem / should_be, 1.0, delta=0.0001)

    def test_pfft(self):
        """
        Tests that the sum of pfft elements is correct
        """
        fft_size = 4096
        sample_rate = 44100
        freq = 3000
        pfft = PFFT(fft_size / 2)

        sine_wave = np.fromfunction(
            lambda x: sin(x * 2 * pi * freq / sample_rate),
            (fft_size,))

        sine_pfft = pfft.pfft(sine_wave)
        self.assertAlmostEqual(np.sum(sine_pfft), 1.0, delta=0.001)

        sine_pfft = pfft.pfft(sine_wave / 2)
        self.assertAlmostEqual(np.sum(sine_pfft), 0.25, delta=0.001)

    def do_test_sine_waves(self, sample_rate, fft_size, freq,
                           signal, noise, delta):
        """
        Helper function that tests that noise is just audible in the presence
        of the signal of the same frequency, given the amplitudes.
        """
        pfft = PFFT(fft_size / 2)
        model = PsyModel(sample_rate / 2, fft_size / 2)

        sine_wave = np.fromfunction(
            lambda x: sin(x * 2 * pi * freq / sample_rate),
            (fft_size,))

        sine_pfft = pfft.pfft(sine_wave)

        audibility = model.distortion_audibility(
            (signal ** 2) * sine_pfft,
            (noise ** 2) * sine_pfft)

        self.assertAlmostEqual(audibility, 1.0, delta=delta)

    # XXX: the next three tests don't pass if the frequency is changed.
    # Is this expected?
    def test_17dB_rule(self):
        """
        Tests that, at 1 kHz, a 53 dB SPL sine wave is just noticeable
        as a distortion of a 70 dB SPL sine wave, as claimed in Section 3
        "Calibration of the Model" of the paper
        """
        signal = 10 ** ((70 - 92) / 20.0)
        noise = 10 ** ((53 - 92) / 20.0)
        self.do_test_sine_waves(44100, 1024, 1000, signal, noise, delta=0.01)

    def test_ATH_audibility(self):
        """
        Tests that, without signal, a "distortion" at the absolute
        threshold of hearing is just noticeable
        """
        noise = PsyModel.ATH(1000)
        self.do_test_sine_waves(44100, 1024, 1000, 0, noise, delta=0.01)

    def test_fft_size_invariance(self):
        """
        Tests that the model works for FFT sizes other than 1024
        """
        noise = PsyModel.ATH(1000)
        self.do_test_sine_waves(44100, 4096, 1000, 0, noise, delta=0.1)

    def test_window(self):
        """
        Tests that the side lobes intrduced by the window are not treated as
        an audible distortion
        """
        # Note: the test fails for smaller FFT sizes. Thus, please don't split
        # signal and distortion in such naive way for FFT sizes <= 512.
        sample_rate = 44100
        fft_size = 1024
        freq = 50
        margin = 3
        pfft = PFFT(fft_size / 2)
        model = PsyModel(sample_rate / 2, fft_size / 2)

        sine_wave = np.fromfunction(
            lambda x: sin(x * 2 * pi * freq / sample_rate),
            (fft_size,))
        sine_pfft = pfft.pfft(sine_wave)

        peak_pos = np.argmax(np.abs(sine_pfft))
        is_signal = np.fromfunction(
            lambda p: np.abs(p - peak_pos) <= margin,
            (len(sine_pfft),))

        signal_pfft = sine_pfft * is_signal
        noise_pfft = sine_pfft * (1 - is_signal)

        audibility = model.distortion_audibility(signal_pfft, noise_pfft)
        self.assertLess(audibility, 0.5)


class TestWaveGen(unittest.TestCase):
    def test_lin_sweep(self):
        """
        Verifies that generate_lin_sweep() indeed produces a linear sweep
        with the correct amplitude and length. In particular, that the
        frequency in some midpoint is correct.
        """
        fft_size = 2048
        num_ffts = 4096
        length = fft_size * num_ffts

        self.assertEqual(float_sweep.shape, (length,))
        self.assertAlmostEqual(max(float_sweep), 0.9, delta=0.01)

        # Each 2 * num_ffts samples should correspond to one FFT bin
        test_bin = 1020
        center = test_bin * 2 * num_ffts
        left = center - fft_size / 2
        right = center + fft_size / 2

        sweep_fft = rfft(float_sweep[left:right])

        self.assertAlmostEqual(
            abs(sweep_fft[test_bin]) / fft_size, 0.45, delta=0.01)
        self.assertAlmostEqual(
            abs(sweep_fft[test_bin - 20]) / fft_size, 0, delta=0.01)

    def test_s16_conversion(self):
        """
        Tests that float -> s16 undithered conversion performs proper
        rounding and clipping
        """
        data = np.array([0.0, 0.9, -0.9, 123.4, -123.4, 35000.1, -35000.1])
        expected = np.array([0, 1, -1, 123, -123, 32767, -32768])
        actual = to_s16(data / 32768.0, dither_amount=0.0)
        self.assertTrue((actual == expected).all())

    def test_s16_dither(self):
        """
        Tests that neither naive quantization nor TPDF dithering yield
        audible artifacts at 16 bits.
        """
        # Parameters were adjusted manually to demonstrate a case that should
        # be the worst: third harmonic is at the peak of ear sensitivity,
        # amplitude leads to third-harmonic generation by quantization.
        sample_rate = 44100
        freq = 1200
        fft_size = 4096
        ampl = 1.5 / 32768.0

        sine_wave = np.fromfunction(
            lambda x: ampl * sin(x * 2 * pi * freq / sample_rate),
            (fft_size,))

        undithered_sine_wave = to_s16(sine_wave, dither_amount=0.0)
        dithered_sine_wave = to_s16(sine_wave)

        pfft = PFFT(fft_size / 2)
        model = PsyModel(sample_rate / 2, fft_size / 2)
        quant_distortion = sine_wave - (undithered_sine_wave / 32768.0)
        dither_noise = sine_wave - (dithered_sine_wave / 32768.0)

        sine_pfft = pfft.pfft(sine_wave)
        quant_pfft = pfft.pfft(quant_distortion)
        dither_pfft = pfft.pfft(dither_noise)

        self.assertLess(model.distortion_audibility(sine_pfft, quant_pfft), 1)
        self.assertLess(model.distortion_audibility(sine_pfft, dither_pfft), 1)


class TestResamplerModel(unittest.TestCase):
    def test_fftbin_tracker(self):
        fft_size = 4096
        pieces = 2048

        pfft = PFFT(fft_size / 2)
        tracker = FFTBinTracker(400, 3500)
        for i in xrange(pieces):
            start = i * fft_size
            piece = float_sweep[start:start + fft_size]
            spectrum = pfft.pfft(piece)
            fftbin = tracker.get_fftbin(start, spectrum)
            self.assertAlmostEqual(fftbin, i, delta=1)

        self.assertAlmostEqual(tracker.target_power, 0.81, delta=0.01)

    def test_dropout_detector(self):
        detector = DropoutDetector(0.01, 40)
        piece = float_sweep[1000000:1004096].copy()
        self.assertTrue(detector.has_signal(piece))
        self.assertFalse(detector.has_signal(piece * 0.005))

        self.assertFalse(detector.has_dropout(piece))
        piece[1500:1539] = 0
        self.assertFalse(detector.has_dropout(piece))
        piece[1539] = 0
        self.assertTrue(detector.has_dropout(piece))

    def test_no_resampler(self):
        """
        Tests that the "distortions" introduced by the "copy" resampler
        are not audible for 44100 Hz -> 44100 Hz resampling. In other words,
        tests inaudibility of the dither and the fact that window side-lobes
        are not detected as audible distortions.
        """
        rr = ResamplerResponse(44100, 44100, 4096, s16_sweep)
        ra = rr.to_audibility()
        self.assertLess(np.max(ra.audibility), 1)

    def test_dropout_is_noticeable(self):
        # First, before the high FFT bin
        sweep_with_dropout = s16_sweep.copy()
        sweep_with_dropout[300000:300030] = 0
        rr = ResamplerResponse(44100, 44100, 4096, sweep_with_dropout)
        ra = rr.to_audibility()
        self.assertGreater(np.max(ra.audibility), 10)

        # Then, after that bin
        sweep_with_dropout = s16_sweep.copy()
        sweep_with_dropout[6000000:6000030] = 0
        rr = ResamplerResponse(44100, 44100, 4096, sweep_with_dropout)
        ra = rr.to_audibility()
        self.assertGreater(np.max(ra.audibility), 10)

    def test_consistency(self):
        """
        Tests that ResamplerModel detects the same amount of distortions
        as the direct use of PsyModel.
        """
        sample_rate = 44100
        fft_size = 1024
        fft_bin = 5
        freq = 0.5 * sample_rate * fft_bin / fft_size
        margin = 4

        rr = ResamplerResponse(44100, 44100, 1024, s16_sweep)
        ra = rr.to_audibility()

        pfft = PFFT(fft_size / 2)
        model = PsyModel(sample_rate / 2, fft_size / 2)

        center = len(s16_sweep) * fft_bin / (fft_size / 2)
        piece = s16_sweep[center - fft_size / 2:center + fft_size / 2]
        piece = piece.astype(np.float) / 32768.0
        sine_pfft = pfft.pfft(piece)

        is_signal = np.fromfunction(
            lambda p: np.abs(p - fft_bin) <= margin,
            (len(sine_pfft),))

        signal_pfft = sine_pfft * is_signal
        noise_pfft = sine_pfft * (1 - is_signal)

        audibility1 = model.distortion_audibility(signal_pfft, noise_pfft)
        audibility2 = ra.audibility[fft_bin]
        self.assertAlmostEqual(audibility1, audibility2, delta=0.1)

    def test_linear_interpolation(self):
        """
        Tests that the model notices the distortion introduced by a
        linear-interpolation resampler.
        """
        xs = np.linspace(0.0, 4096.0 * 2048 - 1, 4096 * 2048)
        xs_resampled = np.linspace(0.0, 4096.0 * 2048 - 1,
                                   4096 * 2048 * 48000 / 44100)
        resampled = np.interp(xs_resampled, xs, float_sweep)

        rr = ResamplerResponse(48000, 44100, 1024, resampled)
        ra = rr.to_audibility()
        self.assertGreater(np.max(ra.audibility), 50)


class NoiseTest(unittest.TestCase):
    def test_rebin_1(self):
        orig = np.array([1.0, 2.0, -1.0])
        new = rebin(orig, 6, 1.2)
        expected = np.array([0.6, 0.8, 1.2, 0.0, -0.6, 0.0])
        self.assertLess(np.max(np.abs(new - expected)), 1e-6)

    def test_rebin_2(self):
        orig = np.array([1.0, 2.0, -1.0])
        new = rebin(orig, 3, 1.2)
        expected = np.array([1.4, 1.2, -0.6])
        self.assertLess(np.max(np.abs(new - expected)), 1e-6)

    def test_pfft(self):
        # A 1000 Hz wave sampled at 48 kHz
        wave = np.fromfunction(
            lambda t: np.sin(2 * 1000 * np.pi * t / 48000),
            (480000,))
        model = NoiseModel(48000, 1024, wave, dba=62, needed_rate=44100)
        self.assertLess(model.pfft[512], 1e-6)
        self.assertAlmostEqual(np.sum(model.pfft), 0.001, delta=1e-5)


def setUpModule():
    global float_sweep
    global s16_sweep
    logging.disable(logging.ERROR)
    float_sweep = generate_lin_sweep(4096 * 2048, 0.9)
    s16_sweep = to_s16(float_sweep)

if __name__ == '__main__':
    unittest.main()
