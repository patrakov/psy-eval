# Copyright (c) 2014 Alexander E. Patrakov. All rights reserved.
# Loosely based on the source of scipy.io.wavfile which is:
#   Copyright (c) 2001, 2002 Enthought, Inc. All rights reserved.
#   Copyright (c) 2003-2013 SciPy Developers. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from subprocess import Popen, PIPE
from threading  import Thread
import struct
import numpy as np

class FFMpegSource(object):
    def __init__(self, filename):
        self.channels = None
        self.rate = None
        args = ['ffmpeg', '-nostats', '-y', '-vn', '-sn', '-i', filename,
                '-f', 'wav', '-acodec', 'pcm_s16le', '-']
        self.pipe = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        self.pipe.stdin.close()
        self.stderr = ''
        self.stderr_reader = Thread(target=self.consume_stderr)
        self.stderr_reader.daemon = True
        self.stderr_reader.start()
        try:
            self.read_header()
        except:
            self.close()
            raise

    def read(self, max_amount):
        return self.pipe.stdout.read(max_amount)

    def read_samples(self, max_amount):
        raw_bytes = self.read(max_amount * self.channels * 2)
        raw_samples = np.fromstring(raw_bytes, dtype=np.int16)
        return raw_samples.reshape((-1, self.channels))

    def close(self):
        self.pipe.terminate()
        self.pipe.wait()
        self.stderr_reader.join()

    def consume_stderr(self):
        while True:
            data = self.pipe.stderr.read(4096)
            if not data:
                break
            self.stderr += data

    def read_riff_header(self):
        header = self.read(12)
        if not header:
            raise ValueError('FFMpeg failed to produce any output')
        riff, length, wave = struct.unpack('<4sL4s', header)
        if (riff, wave) != ('RIFF', 'WAVE'):
            raise ValueError('FFMpeg failed to produce a valid RIFF header')

    def read_chunk_header(self):
        header = self.read(8)
        chunkid, length = struct.unpack('<4sL', header)
        return chunkid, length

    def read_fmt(self, length):
        if length < 16:
            raise ValueError('FFMpeg failed to produce a valid fmt chunk length')

        chunk = self.read(16)
        comp, noc, rate, sbytes, ba, bits = struct.unpack('HHIIHH', chunk)
        self.channels = noc
        self.rate = rate
        if comp not in (1, 65534):
            raise ValueError('FFMpeg failed to produce a PCM wav file')

        if (sbytes, ba, bits) != (rate * noc * 2, noc * 2, 16):
            raise ValueError('FFMpeg failed to produce a 16-bit PCM wav file')
        self.skip_chunk(length - 16)

    def skip_chunk(self, length):
        if length:
            data = self.read(length)
            if len(data) != length:
                raise ValueError('Unexpected end of file')

    def read_header(self):
        self.read_riff_header()
        while True:
            chunkid, length = self.read_chunk_header()
            if chunkid == 'data':
                if self.channels is None:
                    raise ValueError('FFMpeg failed to produce a fmt chunk')
                break
            if chunkid == 'fmt ':
                self.read_fmt(length)
            else:
                self.skip_chunk(length)
