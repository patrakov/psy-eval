#!/usr/bin/env python2
# coding: utf-8
# Copyright (c) 2014 Alexander E. Patrakov. All rights reserved.
# Copyright (c) 2014 Damir Jelić. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import sys
import argparse

import numpy as np
from numpy import sin, pi

from scipy.io import wavfile


def generate_lin_sweep(length, amplitude):
    """
    Generates a linear sine sweep from 0 Hz to half of the sample rate,
    with a specified amplitude (where 1.0 is taken to be the full scale)
    and duration in samples.
    """
    sweep_rate = 0.5 / length
    samples = np.fromfunction(
        lambda s: amplitude * sin(2 * pi * sweep_rate * s * s / 2),
        (length,))
    return samples


def generate_sine_wave(length, amplitude, period):
    """
    Generates a sine wave with a given amplitude, period and duration
    in samples
    """
    samples = np.fromfunction(
        lambda s: amplitude * sin(2 * pi * s / period),
        (length,))
    return samples

def to_s16(samples, dither_amount=1.0):
    """
    Converts floating-point samples to s16, with TPDF dithering.
    dither_amount exists only for the tests.
    """
    dither1 = np.random.random(samples.shape)
    dither2 = np.random.random(samples.shape)
    dither = dither1 - dither2

    scaled = samples * 32768.0
    dithered = scaled + dither_amount * dither
    clipped = np.clip(dithered, -32768, 32767)
    rounded = np.rint(clipped)
    return rounded.astype(np.int16)


def parse_args():
    parser = argparse.ArgumentParser(description='Signal generator')

    parser.add_argument('--rate', help='Sample rate of the generated signal',
                        default=44100, type=int)
    parser.add_argument('--length', help='Duration of the signal in samples',
                        type=int, default=4096 * 4096)
    parser.add_argument('--amplitude', help='Amplitude of the signal',
                        type=float, default=0.5)
    parser.add_argument('--format',
                        help='Sample format (currently supported: s16, float)',
                        default='s16')
    parser.add_argument('--padding',
                        help='Pad the output with this number of zeros on both sides',
                        type=int, default=0)
    parser.add_argument('--constant-freq',
                        help='Generate a sine wave of the constant frequency '
                        '(specified in Hz) instead of a linear sweep',
                        type=int)
    parser.add_argument('file',
                        help='File where to save the generated signal')

    return parser.parse_args()


def check_settings(args):
    if args.format not in ('s16', 'float'):
        raise ValueError('Invalid sample format!')

    if args.rate <= 0:
        raise ValueError('Invalid sample rate!')

    if not 0 <= args.amplitude <= 1:
        raise ValueError('Invalid amplitude!')


def main():
    args = parse_args()

    try:
        check_settings(args)
    except ValueError as e:
        print >> sys.stderr, e.args[0]
        return

    fname = args.file
    rate = args.rate
    length = args.length
    amplitude = args.amplitude
    pad = args.padding

    if args.constant_freq is None:
        samples = generate_lin_sweep(length, amplitude)
    else:
        period = 1.0 * rate / args.constant_freq
        samples = generate_sine_wave(length, amplitude, period)

    if args.format == 's16':
        samples = to_s16(samples)
    elif args.format == 'float':
        samples = samples.astype(np.float32)

    if pad:
        padding = np.zeros((pad,), dtype=samples.dtype)
        samples = np.hstack((padding, samples, padding))

    wavfile.write(fname, rate, samples)


if __name__ == "__main__":
    main()
