#!/usr/bin/env python2
# Copyright (c) 2014 Alexander E. Patrakov. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import numpy as np
import matplotlib.pyplot as p
import matplotlib.ticker as mticker

from ffmpeg import FFMpegSource
from utils import PFFT
from psymodel import PsyModel
from resampler_plots import save_plot, create_noise_model, create_resampler_model

import argparse


def parse_args():
    parser = argparse.ArgumentParser(description='Music distortion evaluator')

    parser.add_argument('--resampler-response',
                        help='wav file with resampler response',
                        required=True)
    parser.add_argument('--rate-from',
                        help='Sample rate of the original signal',
                        type=int, required=True)
    parser.add_argument('--rate-to',
                        help='Sample rate of the resampled signal',
                        type=int)
    parser.add_argument('--skip',
                        help='Skip this number of samples in resampler response',
                        type=int, default=0)
    parser.add_argument('--fftsize', help='FFT size', type=int, default=1024)
    parser.add_argument('--noise-file', help='wav file with room noise (optional)')
    parser.add_argument('--noise-full-scale',
                        help='Sound pressure level, in dB, corresponding to the '
                        'full scale in the noise file (default: 92)',
                        type=float)
    parser.add_argument('--noise-dba',
                        help='A-weighed sound pressure level, in dB, corresponding '
                        'to the noise file (i.e. what the noise meter says)',
                        type=float)
    parser.add_argument('--signal-full-scale',
                        help='Sound pressure level, in dB, corresponding to the '
                        'full scale of the resampled signal (default: 92)',
                        type=float)
    parser.add_argument('--use-eq',
                        help='Ignore wrong amplitude of resampled sound (default: false)',
                        action='store_true', default=False)
    parser.add_argument('--save', help='Prefix of the names of plot files')
    parser.add_argument('--report-only',
                        help="Don't plot anything, just report statistics to stdout",
                        action='store_true', default=False)
    parser.add_argument('file', help='Music file in any format supported by ffmpeg')

    return parser.parse_args()


def minsec(sec, unused):
    minutes = sec // 60
    sec = sec - minutes * 60
    return '%d:%02d' % (minutes, sec)


def main():
    args = parse_args()
    fs = FFMpegSource(args.file)
    rm = create_resampler_model(args, args.resampler_response)
    nm = create_noise_model(args, rm.sample_rate)

    pfft = PFFT(rm.fftbin_max)

    model = PsyModel(fs.rate / 2.0, rm.fftbin_max)
    model.set_resampler_model(rm, use_eq=args.use_eq)

    if nm is not None:
        model.set_noise_model(nm)

    chunk_size = rm.fftbin_max
    seconds_per_chunk = 1.0 * chunk_size / fs.rate

    history = np.zeros(chunk_size, dtype=np.float)

    scale = 1 / 32768.0
    if args.signal_full_scale is not None:
        scale *= 10 ** ((args.signal_full_scale - 92.0) / 20)

    aud_vs_time = []
    while True:
        chunk = fs.read_samples(chunk_size).astype(np.float) * scale
        if len(chunk) < chunk_size:
            break

        if len(chunk.shape) == 2:
            chunk = chunk[:,0]

        chunk_pfft = pfft.pfft(np.hstack((history, chunk)))
        aud_vs_time.append(model.resampler_distortion_audibility(chunk_pfft))
        history = chunk

    aud_vs_time = np.array(aud_vs_time)

    aud_avg = np.average(aud_vs_time)
    aud_max = np.max(aud_vs_time)
    aud_argmax = np.argmax(aud_vs_time)
    aud_argmax = aud_argmax * seconds_per_chunk

    print '"%s", average distortion = %2.1f dB, maximum = %2.1f dB, at %s' % (
            args.file, 10 * np.log10(aud_avg), 10 * np.log10(aud_max), minsec(aud_argmax, None)
        )

    if args.report_only:
        return

    xs = np.linspace(0, (len(aud_vs_time) - 1.0) * seconds_per_chunk, len(aud_vs_time))

    locator = mticker.MultipleLocator(60)
    formatter = mticker.FuncFormatter(minsec)

    fig, ax = p.subplots()
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)

    ax.set_ylim(-30, 20)

    p.plot(xs, 10 * np.log10(aud_vs_time))
    p.axhline(0, color='k')

    p.xlabel('Time, min:sec')
    p.ylabel('Distortion audibility, dB')

    save_plot(args, '_audibility_vs_time.png')


if __name__ == '__main__':
    main()
