# Copyright (c) 2014 Alexander E. Patrakov. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import numpy as np

# some universal functions
from numpy import exp, fmin, fmax

from numpy.fft import rfft

from utils import PFFT
# In the whole file, all frequencies in function parameters are in Hz.


# Cut-off frequencies, from https://en.wikipedia.org/wiki/Bark_scale
# Two points at the end are from
# https://ccrma.stanford.edu/~jos/bbt/Bark_Frequency_Scale.html
# ...so that sample rates above 31 kHz can be supported.
CRITICAL_BANDS = np.array([
    20, 100, 200, 300, 400, 510, 630, 770, 920, 1080,
    1270, 1480, 1720, 2000, 2320, 2700, 3150, 3700, 4400, 5300,
    6400, 7700, 9500, 12000, 15500, 20500, 27000
])


# CS and CA are constants from Eq. 5 from
# http://www.mp3-tech.org/programmer/docs/6_Heusdens.pdf
# The actual values were guessed manually in order to pass the tests
# test_17dB_rule and test_ATH_audibility
CS = 2.3
CA = 2.436


class PsyModel(object):
    """
    A psychoacoustical model based on gamma-tone filterbank
    See http://www.mp3-tech.org/programmer/docs/6_Heusdens.pdf
    """
    def __init__(self, freq_end, freq_bands):
        self.freq_end = freq_end
        self.freq_bands = freq_bands
        self.filter_matrix = self.make_filter_matrix(freq_end, freq_bands)
        self.noise_exc = None

    def set_resampler_model(self, resampler_model, use_eq):
        """
        Informs the psychoacoustical model about distortions
        introduced by the resampler.
        """
        assert self.freq_bands == resampler_model.fftbin_max
        dst_rate = resampler_model.sample_rate
        dst_fft_size = resampler_model.fft_size
        dst_filter_matrix = self.make_filter_matrix(dst_rate / 2, dst_fft_size / 2)
        if use_eq:
            distortion_matrix = resampler_model.distortion_matrix_eq
        else:
            distortion_matrix = resampler_model.distortion_matrix
        self.distortion_filter_matrix = np.dot(distortion_matrix, dst_filter_matrix)

    def distortion_audibility(self, signal_pfft, distortion_pfft):
        """
        Evaluates the masking of distortion by signal. Returns a number
        that the power of the distortion should be divided by so that
        it becomes just-noticeable. I.e. if the returned value is less than
        1, then a human cannot notice this distortion.
        """
        assert signal_pfft.dtype.kind == 'f'
        assert distortion_pfft.dtype.kind == 'f'

        assert signal_pfft.shape == (self.freq_bands + 1,)
        assert distortion_pfft.shape == (self.freq_bands + 1,)

        signal_exc = np.dot(signal_pfft, self.filter_matrix)
        dist_exc = np.dot(distortion_pfft, self.filter_matrix)

        if self.noise_exc is not None:
            signal_exc += self.noise_exc

        return CS * np.sum(dist_exc / (signal_exc + CA))

    def resampler_distortion_audibility(self, signal_pfft):
        assert signal_pfft.dtype.kind == 'f'
        assert signal_pfft.shape == (self.freq_bands + 1,)

        signal_exc = np.dot(signal_pfft, self.filter_matrix)
        dist_exc = np.dot(signal_pfft, self.distortion_filter_matrix)

        return CS * np.sum(dist_exc / (signal_exc + CA))

    @staticmethod
    def ATH_dB_Terhardt(freq):
        """
        Returns the absolute threshold of hearing, in dB SPL,
        at frequency freq
        calculated according to:
        E. Terhardt, "Calculating virtual pitch", Hearing Research,
        vol. 1, pp. 155-182, 1979
        """

        f = freq / 1000.0

        return (3.64 * f ** (-0.8)
                - 6.5 * exp(-0.6 * (f - 3.3) ** 2)
                + 0.001 * f ** 4)

    @staticmethod
    def ATH_dB_Lame(freq):
        """
        Returns the absolute threshold of hearing, in dB SPL,
        at frequency freq
        calculated according to the same formula as used in LAME:
        http://marc.info/?l=mp3encoder&m=115246004422078&w=3
        """

        f = freq / 1000.0

        return (3.64 * f ** (-0.8)
                - 6.8 * exp(-0.6 * (f - 3.4) ** 2)
                + 6.0 * exp(-0.6 * (f - 8.7) ** 2)
                + 0.0006 * f ** 4)

    ATH_dB = ATH_dB_Lame

    @staticmethod
    def ATH(freq):
        """
        Returns the absolute threshold of hearing, as a floating-point
        sample value, at frequency freq
        Assumes that sample value 1.0 corresponds to 92 dB SPL
        """
        freq = fmin(fmax(freq, 20), 20000)
        dB = PsyModel.ATH_dB(freq)
        return 10.0 ** ((dB - 92) / 20)

    @staticmethod
    def gammatone_filter(bark, freq):
        """
        Returns the magnitude of the transfer function of the gamma-tone
        filter describing the sensitivity of the basilar membrane, for
        pitch bark and frequency freq (that do not need to correspond).
        """
        # See Eq. (1) in http://www.mp3-tech.org/programmer/docs/6_Heusdens.pdf
        freq_center = (CRITICAL_BANDS[bark] + CRITICAL_BANDS[bark - 1]) / 2.0
        bandwidth = CRITICAL_BANDS[bark] - CRITICAL_BANDS[bark - 1]

        fnorm = (freq - freq_center) / 1.019 / bandwidth
        in_denom = 1 + fnorm * fnorm
        return 1 / (in_denom * in_denom)

    @staticmethod
    def filter_product(bark, freq):
        prod = PsyModel.gammatone_filter(bark, freq) / PsyModel.ATH(freq)
        return prod * prod

    @staticmethod
    def make_filter_matrix(freq_end, freq_bands):
        """
        Returns a 2D array that can be used to convert a vector of
        squared Fourier coefficients of masker or noise to a vector
        of excitations for each filter
        """
        return np.fromfunction(
            lambda f, b:
                PsyModel.filter_product(b + 1, freq_end * f / freq_bands),
            (freq_bands + 1, len(CRITICAL_BANDS) - 1), dtype=np.int)

    def set_noise_model(self, noise_model):
        noise_filter_matrix = self.make_filter_matrix(
            noise_model.sample_rate / 2, noise_model.fft_size / 2)
        self.noise_exc = np.dot(noise_model.pfft, noise_filter_matrix)
